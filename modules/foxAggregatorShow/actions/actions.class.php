<?php

require_once dirname(__FILE__).'/../lib/BasefoxAggregatorShowActions.class.php';

/**
 * foxAggregatorShow actions.
 * 
 * @package    sfFoxAggregatorShowPlugin
 * @subpackage foxAggregatorShow
 * @author     Consulting Alpha
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class foxAggregatorShowActions extends BasefoxAggregatorShowActions
{
}
