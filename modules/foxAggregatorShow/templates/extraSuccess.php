<?php slot('title', $extra) ?>

<div class="shadow">
    <!-- COVER -->
    <?php include_partial('showCover',Array('show'=>$show, 'aggregator' => $aggregator))?>
    <!-- FINE COVER  -->

    <div class="content">

      <div class="module620 left" id="anchor">

        <h4><span class="fontNormal"><?php echo strtoupper($show);?></span><br /><?php echo __("GLI EXTRA"); ?></h4>

          <hr class="separator" />

          <div id="panelExtra">

            <ul>

              <?php foreach($extras as $k => $extra): ?>

              <li id="cast-item<?php echo $k?>">

                <div class="module200 left thumb200x110Cont">
                    <a href="<?php echo url_for('@showExtraDectail?slug=' . $show->getI18nSlug() . '&slugExtra=' . $extra->getI18nSlug());?>" title="<?php echo __("%show% - %extra%", Array('%show%' => $show->getName(), '%extra%' => $extra->getTitle()))?>" style="overflow-x: hidden; overflow-y: hidden; opacity: 1; "><img src="<?php echo $extra->getImage('190x100'); ?>" title="<?php echo __("%show% - %extra%",Array('%show%' => $show->getName(), '%extra%' => $extra->getTitle()))?>"  /></a>
                </div>
                <div class="module380 left marginLeft10">
                    <h4 class="titolo"><a href="<?php echo url_for('@showExtraDectail?slug=' . $show->getI18nSlug() . '&slugExtra=' . $extra->getI18nSlug());?>" title="<?php echo $extra->getTitle()?>"><?php echo $extra->getTitle()?></a></h4>
                    <p class="descrizione"><?php echo $extra->getAbs()?></p>
                </div>
                <div class="clear"></div>

              </li>

              <?php endforeach; ?>

            </ul>

          </div>

        </div>

        <div class="module300 right">
			<?php include_partial('show/showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
        </div>

        <div class="clear"></div>

    </div>
</div>
<div class="footer"></div>
