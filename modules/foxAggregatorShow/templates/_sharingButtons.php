<?php
    $path = $sf_request->getUri();
?>


<div class="share marginTop10 right">
  <a href="javascript:void(0)" rel="http://www.facebook.com/sharer.php?u=<?php echo $path; ?>" title="<?php echo __("Condividi su Facebook"); ?>" class="left marginRight10 fb-button"><?php echo __("CONDIVIDI SU FACEBOOK"); ?></a>
  <a href="javascript:void(0)" rel="http://twitter.com/share?original_referer=<?php echo $path?>" title="<?php echo __("Condividi su Twitter"); ?>" class="left fb-twitter"><?php echo __("CONDIVIDI SU TWITTER"); ?></a>
  <div class="clear"></div>
</div>

