<?php slot('title', $show . ' - guida TV') ?>

<div class="shadow">

  <!-- COVER -->
  <?php include_partial('showCover',Array('show'=>$show,'aggregator' => $aggregator))?>
  <!-- FINE COVER  -->

  <div class="content">

        <div class="module620 left" id="anchor">

            <?php if(count($programs) <1):?>
                <div class='noTvGuide'>NON IN PROGRAMMAZIONE </div>
            <?php else: ?>
                <h4 class="marginBottom20"><span class="fontNormal"><?php echo strtoupper($show->getName()); ?></span><br><?php echo strtoupper(__("PROGRAMMAZIONE")); ?></h4>
                <hr class="separator">
                <div id="panelGuidaShow">
                    <ul>
                      <?php foreach($programs->getResults() as $program): ?>

                        <li id="guidatv-item1">
                            <div class="orario left">
                                <div class="firstline"><?php echo ucfirst(format_date($program->getDatetime(),'EEEE')); ?><br><?php echo format_date($program->getDateTime(),'d MMMM'); ?></div>
                                <div class="secondline"><?php echo format_date($program->getDatetime(),'HH:mm');?></div>
                            </div>
                            <div class="scheda left paddingLeft10">
                                <!--p class="occhiello"></p-->
                                <h4 class="titolo"><?php echo ($episode = $program->getEpisode()) ? $episode->getTitle() : $program->getShow()->getName()?></h4>
                                <p class="descrizione">
                                  <?php
                                  if($episode)
                                  {
                                      echo $episode->getSynopsis();
                                  }
                                  else
                                  {
                                      echo $program->getShow()->getSynopsis();
                                  }
                                  ?>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </li>

                      <?php endforeach; ?>
                    </ul>
                    <?php include_partial('global/pager',Array('pager'=>$programs,'route'=>'@showTvGuide?slug=' . $show->getI18nSlug()));?>
                </div>
            <?php endif; ?>

        </div>

        <div class="module300 right">
        	<?php include_partial('show/showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>true, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
        </div>

        <div class="clear"></div>

    </div>

</div>
<div class="footer"></div>
