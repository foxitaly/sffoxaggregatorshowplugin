
<div id="panel" class="marginTop30">
  <div class="guida-header-show"></div>
  <div class="guida-content-show" >

  <?php if(@count($programs)): ?>

    <?php foreach($programs as $program): ?>

    <div class="marginBottom20">
        <div class="module60 left">
            <small class="size11 fontBold greyText"><?php echo ucfirst(format_date($program->getDatetime(),'EEE dd/MM'));?></small><br />
            <h3 class="Futura size20 greyText"><?php echo format_date($program->getDatetime(),'HH.mm')?></h3>
        </div>
        <div class="module20 left"><img src="/images/historychannel/panels/guidatv-graffa.png" title="separator" alt="" /></div>
        <div class="module200 left">
            <p class="margin0 size10 uppercase"><?php echo $program->getShow(); ?></p>
            <h5 class="size12 uppercase margin0"><?php echo ($episode = $program->getEpisode()) ? $episode->getTitle() : ''?></h5>
        </div>
        <div class="clear"></div>
    </div>

    <?php endforeach; ?>

  <?php else: ?>
  
    <p><?php echo __("NON IN PROGRAMMAZIONE"); ?></p>

  <?php endif; ?>
  </div>
  <div class="guida-call-to-action"><a href="<?php echo url_for('@showTvGuide?slug=' . $show->getI18nSlug())?>" title="<?php echo __("FoxCrime | Guida tv completa"); ?>"><?php echo __("GUIDA TV COMPLETA"); ?></a></div>
</div>


