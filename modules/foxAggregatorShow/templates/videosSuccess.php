<?php slot('linkrel', '<link rel="video_src" href="http://fox.crosscast-system.com/embed/?v=' . $selectedVideo->getCrosscastId() . '&s=history" />') ?>
<?php $sf_response->addMeta('og:image', 'http://85.94.205.192/fox.crosscast-system.com/Images/' . $selectedVideo->getCrosscastId() . '/m_thumbnail.jpeg') ?>
<?php use_javascript('vote') ?>

<div class="shadow">

  <!-- COVER -->

  <?php include_partial('showCover', array('show' => $show,'aggregator' => $aggregator)) ?>

  <!-- FINE COVER  -->


  <div class="content">

    <div class="module620 left" id="anchor">

      <h4><span class="fontNormal"><?php echo strtoupper($show->getName()); ?> - <?php echo $selectedVideo->getShow()->getName();?></span><br><?php echo __("VIDEO, ESTRATTI E PROMO"); ?></h4>

      <hr class="separator">
		
      <iframe src="<?php echo $selectedVideo->getVideoPlayerUrl(); ?>" type="text/html" width="620" height="450" frameborder="0" allowtransparency="true"></iframe>

      <?php include_partial('main/voteVideo',Array('selectedVideo'=>$selectedVideo,'rating'=>$rating));?>

      <?php include_partial('sharingButtons',Array('title'=>$selectedVideo->getTitle())); ?>

      <div class="clear"></div>

      <hr class="marginTop20">

        <!--div id="menuVideo" class="marginTop20">

          <ul>
              <li><a href="#" titolo="Titolo del link" class="selected">Tutti i video</a></li>
              <li><a href="#" titolo="Titolo del link">Video più visti</a></li>
              <li><a href="#" titolo="Titolo del link">Video più votati</a></li>
              <div class="clear"></div>
          </ul>

        </div-->

          <div id="panelVideo">

              <ul>
                  <?php foreach($allVideos as $k => $video): ?>

                  <?php $isLast     = ($k%3 == 2) ? true : false; ?>
                  <?php $isSelected = ($video == $selectedVideo) ? true : false; ?>
                  <?php $appendPage = ($sf_request->getParameter('page')) ? '&page=' . $sf_request->getParameter('page') : '';?>

                  <li class="foto<?php echo ($isLast) ? ' last' : '';?> <?php echo ($isSelected) ? ' selected' : ''; ?>">
                      <div class="iconaPlay"><img src="/images/historychannel/panels/video-icona-play.png"></div>
                      <a href="<?php echo url_for('@showVideo?slug=' .$show->getI18nSlug() . '&slugVideo=' . $video->getI18nSlug() . $appendPage); ?>" title="<?php echo $video->getTitle(); ?>"><img src="<?php echo $video->getVideoThumb(Video::M);?>" title="<?php echo $video->getTitle(); ?>" alt="<?php echo $video->getTitle(); ?>"></a>
                      <p><a href="<?php echo url_for('@showVideo?slug=' .$show->getI18nSlug() . '&slugVideo=' . $video->getI18nSlug() . $appendPage); ?>" title="<?php echo $video->getTitle()?>"><?php echo $video->getTitle(); ?></a></p>
                  </li>

                  <?php if($isLast): ?>

                  <div class="clear marginBottom30"></div>

                  <?php endif; ?>

                  <?php endforeach; ?>
              </ul>

            <?php include_partial('global/pager',Array('pager'=>$videoPager,'route'=>'@showVideo?slug=' . $show->getI18nSlug()));?>


          </div>

    </div>

    <div class="module300 right">
    	<?php include_partial('showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>true)); ?>
    </div>

    <div class="clear"></div>

  </div>

</div>

<div class="footer"></div>
