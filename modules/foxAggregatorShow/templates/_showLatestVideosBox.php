<?php if(@count($videos) && is_array($videos)): ?>

<?php if($videos): ?>

<div id="panel" class="marginTop30">
    <div class="video-header"></div>

    <div class="video-content">
      <?php foreach($videos as $k => $video): ?>
      <div class="foto <?php echo ($k%2 == 0) ? "marginRight20" : ""; ?> marginBottom20 left">
          <div class="iconaPlay">
              <img src="/images/historychannel/panels/video-icona-play.png" alt="" />
          </div>
          <a href="<?php echo url_for('@showVideo?slug=' . $show->getI18nSlug() . '&slugVideo=' . $video->getI18nSlug())?>" title="<?php echo __("%show% - %video%", Array('%show%' => $show->getName(), '%video%' => $video->getTitle()));?>" class="foto">
            <div style="overflow-x: hidden; overflow-y: hidden; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; font-size: 0.1px; width: 134px; height: 100px; margin-top: 0px; border-top-style: none; border-top-width: 0px; border-top-color: rgb(209, 0, 0); margin-right: 0px; border-right-style: none; border-right-width: 0px; border-right-color: rgb(209, 0, 0); margin-bottom: 0px; border-bottom-style: none; border-bottom-width: 0px; border-bottom-color: rgb(209, 0, 0); margin-left: 0px; border-left-style: none; border-left-width: 0px; border-left-color: rgb(209, 0, 0); " class="caption-wrapper">
              <img width='134' src="<?php echo $video->getVideoThumb();?>" title="<?php echo $video->getTitle();?>" alt="<?php echo $video->getTitle(); ?>" class="captify opacity80" rel="caption1" id="foto" style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-color: initial; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; " />
              <div class="caption-bottom" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; z-index: 1; position: relative; opacity: 0.7; width: 144px; height: 26px; margin-top: 0px; "></div>
              <div class="caption-bottom" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 4px; position: relative; z-index: 2; background-image: none; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: initial; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-color: initial; opacity: 1; width: 100%; margin-top: -36px; background-position: initial initial; background-repeat: initial initial; ">
                <div class="smallSize lightText paddingRight20" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; "><?php echo $video->getTitle()?></div>
              </div>
            </div>
          </a>

      </div>

      <?php if($k%2 == 1): ?>

      <div class="clear"></div>

      <?php endif; ?>

      <?php endforeach; ?>
    </div>


    <div class="video-call-to-action"><a href="<?php echo url_for('@showVideo?slug=' . $show->getI18nSlug())?>" title="<?php echo __("FoxCrime | Tutti i video"); ?>">TUTTI I VIDEO</a></div>
</div>

<?php endif; ?>

<?php endif; ?>
