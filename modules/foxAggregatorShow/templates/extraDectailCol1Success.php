<div class="shadow">
    <!-- COVER -->
    <?php include_partial('showCover',Array('show'=>$show,'aggregator' => $aggregator))?>
    <!-- FINE COVER  -->
    
    <div class="content">
                    
      <div class="module940" id="anchor">
    
        <h4><span class="fontNormal"><?php echo strtoupper($show->getName()); ?></span><br /><?php echo strtoupper($extra->getTitle());?></h4>
            
          <hr class="separator" />
        
           <?php if($extra->getCrosscastId() != ''): ?>

          <iframe src="<?php echo $extra->getVideoPlayerUrl();?>" width="940" height="600" allowtransparency="true" frameborder="0" marginwidth="0" marginheight="0" hspace="0" vspace="0px" title="embedded content"></iframe>

          <?php elseif($extra->getUrl() != ''): ?>

          <iframe src="<?php echo $extra->getUrl()?>" width="940" height="600" allowtransparency="true" frameborder="0" marginwidth="0" marginheight="0" hspace="0" vspace="0px" title="embedded content"></iframe>

          <?php endif; ?>

                                                                                
        </div>
            
        <!-- GUIDA TV -->

        <div id="panel" class="left">
        	<?php include_component('show','showTvGuideBox',Array('show'=>$show))?>
        </div>
        
        <!-- FINE GUIDA TV -->
                                                            
        <!-- FACEBOOK -->
            
        <div class="marginTop30 facebookFrame left marginLeft20">
            <?php include_partial('main/facebook'); ?>
        </div>
        
        <!-- END FACEBOOK -->
                                    
        <!-- ADV -->
        <div id="panel" class="right marginTop30">
            <?php include_partial('global/banner-interno-top'); ?>
        </div>
        <!-- FINE ADV -->
                            
        <div class="clear"></div>
    
    </div>
</div>
<div class="footer"></div>
