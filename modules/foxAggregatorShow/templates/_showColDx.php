<!-- ADV -->
<?php $noFirstAdv = (!isset($noFirstAdv)) ? false : $noFirstAdv; ?>
<?php if(!$noFirstAdv): ?>
	<div class="marginBottom30"><?php include_partial('global/banner-interno-top'); ?></div>
<?php endif; ?>
<!-- FINE ADV -->

<!-- GUIDA TV -->
<?php $noTvGuide = (!isset($noTvGuide)) ? false : $noTvGuide; ?>
<?php if(!$noTvGuide): ?>
  	<div class="marginBottom30"><?php include_component('titanic100','showTvGuideBox',Array('show'=>$show))?></div>
<?php endif; ?>
<!-- FINE GUIDA TV -->

<!-- EXTRAS -->
<?php $noShowExtraBox = (!isset($noShowExtraBox)) ? false : $noShowExtraBox; ?>
<?php if(!$noShowExtraBox): ?>
	<div class="marginBottom30"><?php include_component('titanic100','showExtraBox',Array('show'=>$show))?></div>
<?php endif; ?>
<!-- FINE EXTRAS -->

<!-- VIDEO -->
<?php $noLastVideo = (!isset($noLastVideo)) ? false : $noLastVideo; ?>
<?php if(!$noLastVideo): ?>
  <div class="marginBottom30"><?php include_component('titanic100','showLatestVideosBox',Array('show'=>$show))?></div>
<?php endif; ?>
<!-- FINE VIDEO -->
                        
<?php $noFb = (!isset($noFb)) ? false : $noFb; ?>
<?php if(!$noFb): ?>
	<div class="marginBottom30"><?php include_partial('main/facebook'); ?></div>
<?php endif; ?>
    
<!-- ADV -->           
<?php $noLastAdv = (!isset($noLastAdv)) ? false : $noLastAdv; ?>
<?php if(!$noLastAdv): ?>
	<div class="marginBottom30"><?php include_partial('global/banner-interno-bottom'); ?></div>
<?php endif; ?>
<!-- FINE ADV -->
