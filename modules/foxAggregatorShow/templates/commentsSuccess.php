<?php slot('title', $show . ' - commenti') ?>

<!-- COVER -->
<?php include_partial('showCover', array('show' => $show, 'aggregator' => $aggregator)) ?>

<div class="content">

  <div class="module620 left">

    <div class="commentList">
      <?php foreach ($pager->getResults() as $comment): ?>

      <div class="<?php echo $pager->isEven() ? 'border' : 'clean' ?>">
        <div class="module60 left">
          <img src="/images/historychannel/icona-utente-generico.jpg" title="icona utente" alt="icona utente" />
        </div>
        <div class="module500 right">
          <h4 class="Futura"><?php echo $comment->getFirstName() . ' ' . $comment->getLastName() ?></h4>
          <small>il <?php echo $comment->getCreatedAt('j/m/Y, \a\l\l\e G:i') ?> ha scritto</small>
          <p><?php echo $comment->getText() ?></p>
        </div>
        <div class="clear"></div>
      </div>

      <?php endforeach ?>
    </div>

    <?php if ($pager->haveToPaginate() && !$pager->isLastPage()): ?>
    <div class="moreComment">
      <a href="<?php echo url_for('@showComments?slug=' . $show->getI18nSlug() . '&page=' . $pager->getNextPage()) ?>" title="Altri commenti">Altri commenti</a>
    </div>
    <?php endif ?>

  </div>

  <div class="module300 right">
  		<?php include_partial('show/showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
  </div>

  <div class="clear"></div>

</div>
