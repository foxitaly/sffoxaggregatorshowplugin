<div class="shadow">
    <!-- COVER -->
    <?php include_partial('show/showCover',Array('show'=>$show))?>
    <!-- FINE COVER  -->
    
    <div class="content">
                    
      <div class="module940" id="anchor">
    
        <h4><span class="fontNormal"><?php echo strtoupper($show->getName()); ?></span>
            
          <hr class="separator" />
                                                                                
        </div>
            
        <!-- GUIDA TV -->

        <div id="panel" class="left">
        	<?php include_component('show','showTvGuideBox',Array('show'=>$show))?>
        </div>
        
        <!-- FINE GUIDA TV -->
                                                            
        <!-- FACEBOOK -->
            
        <div class="marginTop30 facebookFrame left marginLeft20">
            <?php include_partial('main/facebook'); ?>
        </div>
        
        <!-- END FACEBOOK -->
                                    
        <!-- ADV -->
        <div id="panel" class="right marginTop30">
            <?php include_partial('global/banner-interno-top'); ?>
        </div>
        <!-- FINE ADV -->
                            
        <div class="clear"></div>
    
    </div>
</div>
<div class="footer"></div>
