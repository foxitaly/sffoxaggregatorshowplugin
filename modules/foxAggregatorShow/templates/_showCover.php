<?php if($show->hasCard()): ?>

<div id="coverShow">

    <div class="imagesInfo">

        <h1 class="ArialBlack"><?php echo $show->getName();?></h1>

        <?php if($show->getAirDayForCover()): ?>

        <h6><?php echo __("%air_day%  alle %air_time%", Array('%air_day%' => $show->getAirDayForCover(), '%air_time%' => $show->getAirTimeForCover())); ?></h6>
    
        <?php endif; ?>

        <?php if($show->getTagline()): ?>
          <p><?php echo substr($show->getTagline(),0, 300) . "..."?></p>
        <?php endif; ?>

		<hr class="dark" />
        <?php include_partial('socialToolbar'); ?>

          
    </div>
    <div class="imagesOverlay"></div>
    <div style="position:absolute; background:url(/images/historychannel/bg-show-spalla-sx.png) left top no-repeat; height:55px; width:10px; z-index:100; bottom:25px; right:510px;"></div>
    <div style="position:absolute; background:#D10000; height:45px; width:470px; z-index:100; bottom:35px; left:450px;">
      <?php include_partial('showMenu',Array('show'=>$show, 'aggregator'=>$aggregator));?>
    </div>
    <div class="images">
        <img src="<?php echo $show->getCover();?>" width="960" height="350" rel="<?php echo $show->getCover(); ?>" title="<?php echo $show->getName(); ?>" alt="" />
    </div>

</div>
<?php endif; ?>
