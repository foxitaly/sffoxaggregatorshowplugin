<!-- TENDINA -->
<?php use_helper('Text') ?>

<div class="main">
  <div class="module220 left taglio marginRight20">
    <div class="marginLeft10">
      <h3 class="Gotham lightText paddingTop20 paddingBottom20"><?php echo __("NOVITA'") ?></h3>

        <?php if ($isNew): ?>
        <div class="marginBottom10 thumb200x110Cont">
           <a href="<?php echo url_for('@show?slug=' . $isNew->getI18nSlug()) ?>" title="<?php echo $isNew->getName() ?>"><img src="<?php echo $isNew->getCover('190x100') ?>" title="<?php echo $isNew->getName()?>" alt="<?php echo $isNew->getName();?>" /></a>
        </div>
        <h4 class="paddingBottom10 uppercase"><a href="<?php echo url_for('@show?slug=' . $isNew->getI18nSlug()) ?>" title="<?php echo $isNew->getName() ?>"><?php echo $isNew->getName();?></a></h4>
        <p class="size12 lightText"><?php echo truncate_text($isNew->getSynopsis(), 100) ?></p>
        <?php endif ?>
    </div>
  </div>
  <div class="module220 left taglio marginRight20">
    <div class="marginLeft10">
      <h3 class="Gotham lightText paddingTop20 paddingBottom20"><?php echo __("HISTORY PRESENTA") ?></h3>

      <?php foreach ($isRecommended as $k => $recommended): ?>

      <div class="<?php echo ($k < 2) ? 'marginBottom20' : ''  ?>">
        <div class="module60 left thumb60x60Cont">
          <a href="<?php echo url_for('@show?slug=' . $recommended->getI18nSlug()) ?>" title="<?php echo $recommended->getName() ?>"><img src="<?php echo $recommended->getCover('54x54') ?>" title="<?php echo $recommended->getName() ?>" alt="<?php echo $recommended->getName() ?>" /></a>
        </div>
        <div class="module140 right">
          <h5 class="paddingBottom10 uppercase"><a href="<?php echo url_for('@show?slug=' . $recommended->getI18nSlug()) ?>" title="<?php echo $recommended->getName() ?>"><?php echo $recommended->getName() ?></a></h5>
        </div>
        <div class="clear"></div>
      </div>

      <?php endforeach ?>

    </div>
</div>
<div class="module460 left">
  <div class="marginLeft10">
      <h3 class="Gotham lightText paddingTop20 paddingBottom20 uppercase"><?php echo __('IN ONDA SU HISTORY CHANNEL') ?></h3>
        <div class="module220 left">
          <ul class="inonda">
            <?php foreach ($otherShows1 as $k => $other): ?>
            <li><a href="<?php echo url_for('@show?slug=' . $other->getI18nSlug()) ?>" title="<?php echo $other->getName() ?>"><?php echo $other->getName() ?></a></li>
            <?php endforeach ?>
          </ul>
        </div>
        <div class="module220 right">
          <ul class="inonda">
            <?php foreach ($otherShows2 as $k => $other): ?>
            <li><a href="<?php echo url_for('@show?slug=' . $other->getI18nSlug()) ?>" title="<?php echo $other->getName() ?>"><?php echo $other->getName() ?></a></li>
            <?php endforeach ?>
          </ul>
        </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
<div class="footer">
  <a href="<?php echo url_for('@showAll') ?>#top" class="btn-tutte-le-serie right">TUTTE LE SERIE</a>
  <a href="javascript:void(0);" class="btn-close right">CHIUDI</a>
    <div class="clear"></div>
</div>
<!-- FINE TENDINA -->
