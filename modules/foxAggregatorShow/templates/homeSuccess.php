<?php slot('title', $show) ?>
<?php slot('linkrel', '<link rel="image_src" href="' . url_for('@homepage', true) . url_for($show->getCover()) . '" />') ?>
<?php $sf_response->addMeta('og:image', url_for('@homepage', true) . url_for($show->getCover())) ?>
<?php $sf_response->addMeta('og:description', $show->getSynopsis()) ?>
<?php $sf_response->addMeta('og:url', url_for('@show?slug=' . $show->getI18nSlug(), true)) ?>
<?php $sf_response->addMeta('og:site_name', url_for('@homepage', true)) ?>

<meta name="og:title" content="HISTORY CHANNEL solo su SKY canale 407" />
<meta name="og:type" content="tv_show" />

<div class="shadow">
    <!-- COVER -->
    <?php include_partial('showCover',Array('show'=>$show,'aggregator' => $aggregator))?>
    <!-- FINE COVER  -->

    <div class="content">

        <div class="module620 left">

          <!-- TRAMA-->

          <div class="marginBottom20"><img src="/images/historychannel/show/title-trama.png" title="<?php echo __("la trama"); ?>" alt="<?php echo __("la trama"); ?>" /></div>

          <p><?php echo html_entity_decode($show->getSynopsis()); ?></p>

          <!-- FINE TRAMA-->

          <!-- COMMENTI-->
          <?php include_partial('comments', array('show' => $show)); ?>

        </div>

        <div class="module300 right">
			<?php include_partial('showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
        </div>

        <div class="clear"></div>

    </div>
</div>
<div class="footer"></div>
