<?php if(@count($extras)): ?>

<div id="panel" class="marginTop30">
  <div class="extra-header"></div>

  <div class="extra-content">
  <?php foreach($extras as $extra): ?>

    <div class="foto marginRight10 left">
        <a href="<?php echo url_for('@showExtraDectail?slug=' . $show->getI18nSlug() . '&slugExtra=' . $extra->getI18nSlug());?>" title="<?php echo $extra->getTitle()?>"><img src="<?php echo $extra->getImage()?>" title="<?php echo $extra->getTitle()?>" alt="<?php echo $extra->getTitle()?>" width="94" /></a>
    </div>
    <div class="testo left">
        <h4 class="size14 uppercase"><a href="<?php echo url_for('@showExtraDectail?slug=' . $show->getI18nSlug() . '&slugExtra=' . $extra->getI18nSlug());?>" title="<?php echo $extra->getTitle();?>" target="_blank"><?php echo $extra->getTitle()?></a></h4>
        <p><?php echo $extra->getAbs()?></p>
    </div>
    <div class="clear"></div>
    <hr class="separator" />

  <?php endforeach; ?>
  </div>

</div>

<?php endif; ?>
