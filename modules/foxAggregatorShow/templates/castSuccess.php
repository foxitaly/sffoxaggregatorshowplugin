<?php slot('title', $show . ' - CAST') ?>

<div class="shadow">
    <!-- COVER -->
    <?php include_partial('showCover',Array('show'=>$show, 'aggregator' => $aggregator))?>
    <!-- FINE COVER  -->

    <div class="content">

      <div class="module620 left" id="anchor">

        <h4><span class="fontNormal"><?php echo strtoupper($show->getName()); ?></span><br /><?php echo __("IL CAST COMPLETO"); ?></h4>

        <hr class="separator" />

        <div id="panelCast">

          <ul>

              <?php foreach($casts as $character): ?>

              <li id="cast-item<?php echo $character->getSortableRank(); ?>">

                <div class="module200 left thumb200x110Cont">
                  <a href="javascript:void(0);" title="<?php echo $character->getName();?>" style="overflow-x: hidden; overflow-y: hidden; opacity: 1; "-->
                    <img src="<?php echo $character->getImage(); ?>" align="<?php echo $character->getName();?>" title="<?php echo $character->getImage(); ?>" />
                  </a>
                </div>
                <div class="module380 left marginLeft10">
                    <h4 class="titolo"><?php echo $character->getShow()->getName(); ?></h4>
                    <h5><em><?php echo $character->getName()?></em></h5>

                    <?php if($character->hasActor()): ?>

                    <?php foreach($character->getActorCharacters() as $actorCharacter): ?>

                    <p class="occhiello">Interprete: <a href="<?php echo url_for('actor',$actorCharacter->getActor());?>?noLayout=true" title="<?php echo $actorCharacter->getActor()->getName();?>" class="colorbox cboxElement"><?php echo $actorCharacter->getActor()->getName();?></a></p>
                    <?php endforeach; ?>

                    <?php endif; ?>
                    
                    <p class="descrizione"><?php echo $character->getDescription();?></p>

                </div>
                <div class="clear"></div>

              </li>

              <?php endforeach; ?>

          </ul>

        </div>


      </div>

      <div class="module300 right">
      		<?php include_partial('showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
      </div>

      <div class="clear"></div>

    </div>
</div>
<div class="footer"></div>
