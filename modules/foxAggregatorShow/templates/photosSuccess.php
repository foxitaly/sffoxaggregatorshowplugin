<?php slot('title', $show . ' - FOTO') ?>
<?php use_javascript('vote') ?>

<div class="shadow">
  <!-- COVER -->
  <?php include_partial('showCover', array('show' => $show, 'aggregator' => $aggregator)) ?>
  <!-- FINE COVER  -->

  <div class="content">

    <div class="module620 left" id="anchor">

      <h4><span class="fontNormal"><?php echo strtoupper($show->getName()); ?></span><br><?php echo strtoupper(__("LE FOTO")); ?></h4>

      <hr class="separator">

      <div id="gallery">
        <?php $appendPage = ($sf_request->getParameter('page')) ? '&page=' . $sf_request->getParameter('page') : '';?>
        <?php if ($prevPhoto): ?>
        	<a href="<?php echo url_for('@showPhoto?slug=' .$show->getI18nSlug() . '&slugPhoto=' . $prevPhoto->getI18nSlug() . $appendPage)?>" title="<?php echo __("Foto precedente: %photo%",  Array('%photo%' => $prevPhoto->getTitle())); ?>" class="prec"><?php echo __("PRECEDENTE"); ?></a>
		<?php endif; ?>
        
        <?php if ($nextPhoto): ?>
        	<a href="<?php echo url_for('@showPhoto?slug=' .$show->getI18nSlug() . '&slugPhoto=' . $nextPhoto->getI18nSlug() . $appendPage)?>" title="<?php echo __("Foto successiva: %photo%'", Array('%photo%' => $nextPhoto->getTitle())); ?>" class="succ"><?php echo __("SUCCESSIVA"); ?></a>
		<?php endif; ?>
        
        <?php $photoLinked = ($nextPhoto) ? $nextPhoto : $firstPhoto; ?>
        <a href="<?php echo url_for('@showPhoto?slug=' .$show->getI18nSlug() . '&slugPhoto=' . $photoLinked->getI18nSlug() .$appendPage)?>" title="<?php echo __("Foto successiva: %photo%'", Array('%photo%' => $photoLinked->getTitle())); ?>"><img src="<?php echo $selectedPhoto->getImage('608x453'); ?>" alt="<?php echo $selectedPhoto->getTitle();?>" /></a>
      </div>

      <div class="vote marginTop15 left">
        <?php for ($v = 1; $v < 6; $v ++): ?>
        <?php echo link_to($v, '@showVotePhoto?slug=' . $show->getI18nSlug() . '&slugPhoto=' . $selectedPhoto->getI18nSlug() . '&vote=' . $v, 'title=' . $v . ' class=left marginRight10' . ($rating >= $v ? ' active' : '')) ?>
        <?php endfor ?>
        <div class="size10 left" style="line-height:20px">
          <span><?php echo $selectedPhoto->getRatingCount() ?></span> Voti
        </div>
        <div class="clear"></div>
      </div>


      <?php include_partial('global/sharingButtons',Array('title'=>$selectedPhoto->getTitle())); ?>

      <div class="clear"></div>

      <hr class="marginTop20">

      <div id="panelGallery" class="marginTop20">

        <ul>
          <?php foreach ($allPhotos as $k => $photo): ?>

          <?php $isLast     = ($k%4 == 3) ? true : false; ?>
          <?php $isSelected = ($photo == $selectedPhoto) ? true : false; ?>
          <?php $appendPage = ($sf_request->getParameter('page')) ? '&page=' . $sf_request->getParameter('page') : '';?>

          <li class="foto <?php echo ($isLast) ? 'last' : ''?>">
            <a href="<?php echo url_for('@showPhoto?slug=' .$show->getI18nSlug() . '&slugPhoto=' . $photo->getI18nSlug())?>" title="<?php echo $photo->getTitle();?>" class="<?php echo ($photo->getId() == $selectedPhoto->getId()) ? 'selected' : ''; ?>"><img src="<?php echo $photo->getImage('134x100'); ?>" title="<?php echo $photo->getTitle();?>" alt="<?php echo $photo->getTitle();?>" /></a>
          </li>

          <?php endforeach ?>
        </ul>
        <div class="clear"></div>

        <?php include_partial('global/pager',Array('pager'=>$photoPager,'route'=>'@showPhoto?slug=' . $show->getI18nSlug()));?>

      </div>

    </div>

    <div class="module300 right">
    	<?php include_partial('showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>
    </div>

    <div class="clear"></div>

  </div>
</div>
<div class="footer"></div>
