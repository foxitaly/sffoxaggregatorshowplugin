<!--
titanic100Show          ANY       /titanic-100
titanic100ShowProgram   ANY       /titanic-100/programma
titanic100ShowExtra     ANY       /titanic-100/extra
titanic100ShowPhoto     ANY       /titanic-100/foto/:slugPhoto
titanic100ShowPreview   ANY       /titanic-100/anteprima
titanic100ShowVotePhoto ANY       /titanic-100/foto/:slugPhoto/vota/:vote
titanic100ShowTvGuide   ANY       /titanic-100/guidatv
titanic100ShowVideo     ANY       /titanic-100/video/:slugVideo
titanic100ShowVoteVideo ANY       /titanic-100/video/:slugVideo/vota/:vote
titanic100ShowCast      ANY       /titanic-100/cast
titanic100ShowComment   POST      /titanic-100/comment
titanic100ShowComments  ANY       /titanic-100/comments
-->

<ul id="nav">
  <?php if($aggregator->hasCard()): ?><li><a href="<?php echo url_for('@titanic100Show')?>#anchor" title="<?php echo __("%show% - Home", Array('%show%' => $show->getName()))?>" class="<?php echo setToSelected($sf_request,'home') ? 'selected' : ''?>">HOME</a></li><?php endif; ?>
  <?php if($aggregator->hasTvGuide()): ?><li><a href="<?php echo url_for('@showTvGuide?slug=' . $show->getI18nSlug())?>#anchor" title="<?php echo __("%show% - la guida Tv",Array('%show%' => $show->getName()) )?>" class="<?php echo setToSelected($sf_request,'tvguide') ? 'selected' : ''?>">GUIDA TV</a></li><?php endif; ?>
  <?php if($aggregator->hasCasts()): ?><li><a href="<?php echo url_for('@showCast?slug=' . $show->getI18nSlug())?>#anchor" title="<?php echo __("%show% - il cast",Array('%show%' => $show->getName()) )?>" class="<?php echo setToSelected($sf_request,'cast') ? 'selected' : ''?>">CAST</a></li><?php endif; ?>
  <?php if($aggregator->hasPhotos()): ?><li><a href="<?php echo url_for('@showPhoto?slug=' . $show->getI18nSlug())?>#anchor" title="<?php echo __("%show% - le foto",Array('%show%' => $show->getName()) )?>" class="<?php echo setToSelected($sf_request,'photos') ? 'selected' : ''?>">FOTO</a></li><?php endif; ?>
  <?php if($aggregator->hasVideos()): ?><li><a href="<?php echo url_for('@showVideo?slug=' . $show->getI18nSlug())?>#anchor" title="<?php echo __("%show% - i video",Array('%show%' => $show->getName()) )?>" class="<?php echo setToSelected($sf_request,'videos') ? 'selected' : ''?>">VIDEO</a></li><?php endif; ?>

</ul>

