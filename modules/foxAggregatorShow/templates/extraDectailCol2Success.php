<div class="shadow">
    <!-- COVER -->
    <?php include_partial('showCover',Array('show'=>$show, 'aggregator'=>$aggregator))?>
    <!-- FINE COVER  -->
    
    <div class="content">
    
      <div class="module620 left" id="anchor">
    
        <h4><span class="fontNormal"><?php echo strtoupper($show->getName());?></span><br /><?php echo strtoupper($extra->getTitle())?></h4>
    
            <hr class="separator" />
    
            <div id="panelExtraRTF">
    
              <?php foreach($extra->getExtraItems() as $k => $item): ?>
    
              <img src="<?php echo $extra->getImage('200x50');?>" align="left" title="<?php echo $item->getTitle(); ?>" />
              <h4><?php echo $item->getTitle(); ?></h4>
              <p><?php echo $item->getText();?></p>
    
    
              <hr class="marginBottom20">
    
              <?php endforeach; ?>
    
            </div>

            <?php if($extra->hasPhotos()): ?>
    
            <!-- FOTO -->
            <div id="panelExtraFoto">

              <h4 class="Gotham"><?php echo __("FOTO"); ?></h4>
                <hr class="separator" />

                <?php foreach($photoes as $k => $photo): ?>

                <a href="<?php echo $photo->getImage('960x350');  ?>" title="<?php echo $photo->getTitle(); ?>" rel="colorboxFoto" class="cboxElement">
                  <img src="<?php echo $photo->getImage('180x65'); ?>" title="<?php echo $photo->getTitle();?>" />
                </a>

                <?php endforeach; ?>

                <div class="clear"></div>

            </div>
 
            <!-- END FOTO -->

            <?php endif; ?>
    
             <?php if($extra->hasVideos()): ?>
            <!-- VIDEO -->

            <div id="panelExtraVideo">

              <h4 class="Gotham"><?php echo __("VIDEO"); ?></h4>
                <hr class="separator" />

                <?php foreach($videos as $k => $video): ?>

                <a href="<?php echo $video->getVideoPlayerUrl()?>" title="<?php echo $video->getTitle(); ?>" class="colorboxVideo cboxElement">
                  <img src="<?php echo $video->getVideoThumb()?>" title="<?php echo $video->getTitle(); ?>">
                </a>

                <?php endforeach; ?>

                <div class="clear"></div>

            </div>

            <!-- END VIDEO -->
            <?php endif; ?>
   
 
        </div>
    
        <div class="module300 right">
			<?php include_partial('show/showColDx',Array('show'=>$show,'noFirstAdv'=>false, 'noTvGuide'=>false, 'noShowExtraBox'=>false, 'noFb'=>false, 'noLastVideo'=>false)); ?>    
        </div>
    
        <div class="clear"></div>
    
    </div>
</div>
<div class="footer"></div>
