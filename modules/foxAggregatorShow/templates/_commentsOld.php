<?php use_helper('recaptcha') ?>

<div class="commentButton">
  <a href="javascript:void(0);" title="Scrivi un commento" class="btn-commenti">Scrivi un commento</a>
</div>

<div class="commentForm <?php echo $form->hasErrors() ? ' open' : '' ?>">

  <form id="commentForm" title="commentForm" method="post" action="<?php echo url_for('@showComment?slug=' .  $show->getI18nSlug()) ?>">

    <div class="marginBottom10">
      <?php echo $form['first_name']->renderError() ?>
      <?php echo $form['first_name']->renderLabel('Nome', array('class' => 'std')) ?>
      <?php echo $form['first_name'] ?>
    </div>

    <div class="marginBottom10">
      <?php echo $form['last_name']->renderError() ?>
      <?php echo $form['last_name']->renderLabel('Cognome', array('class' => 'std')) ?>
      <?php echo $form['last_name'] ?>
    </div>

    <div class="marginBottom10">
      <?php echo $form['email']->renderError() ?>
      <?php echo $form['email']->renderLabel(null, array('class' => 'std')) ?>
      <?php echo $form['email'] ?>
    </div>

    <div class="marginBottom10">
      <?php echo $form['text']->renderError() ?>
      <?php echo $form['text']->renderLabel('Il tuo commento', array('class' => 'std long')) ?>
      <?php echo $form['text'] ?>
    </div>

    <?php if (sfConfig::get('app_recaptcha_active', false)): ?>
    <div id="captcha">
      <script type="text/javascript">var RecaptchaOptions={lang:'it'}</script>
      <?php echo recaptcha_get_html(sfConfig::get('app_recaptcha_publickey'), $form['response']->getError(), false, true) ?>
    </div>
    <?php endif ?>

    <input type="submit" id="invia" value="INVIA COMMENTO" />
    <a href="javascript:void(0);" title="close comment form" class="btn-close-commenti">CHIUDI</a>
    <div class="clear"></div>

    <?php echo $form->renderHiddenFields() ?>

  </form>

</div>

<div class="commentList">

  <h3 class="Futura">
    <?php if ($pager->count() == 1): ?>
    C'è un commento a questa serie TV
    <?php elseif ($pager->count() > 0): ?>
    Ci sono <?php echo $pager->count() ?> commenti a questa serie TV
    <?php else: ?>
    Non ci sono ancora commenti a questa serie TV
    <?php endif ?>
  </h3>

  <?php if ($pager->count() > 0): ?>

  <?php foreach ($pager->getResults() as $comment): ?>

  <div class="<?php echo $pager->isEven() ? 'border' : 'clean' ?>">
    <div class="module60 left">
      <img src="/images/historychannel/icona-utente-generico.jpg" title="icona utente" alt="icona utente" />
    </div>
    <div class="module500 right">
      <h4 class="Futura"><?php echo $comment->getFirstName() . ' ' . $comment->getLastName() ?></h4>
      <small>il <?php echo $comment->getCreatedAt('j/m/Y, \a\l\l\e G:i') ?> ha scritto</small>
      <p><?php echo $comment->getText() ?></p>
    </div>
    <div class="clear"></div>
  </div>

  <?php endforeach ?>

  <?php endif ?>

</div>

<?php if ($pager->haveToPaginate()): ?>
<div class="moreComment">
  <a href="<?php echo url_for('@showComments?slug=' . $show->getI18nSlug()) ?>" title="Altri commenti">Altri commenti</a>
</div>
<?php endif ?>
