<?php

/**
 * Base actions for the sfFoxAggregatorShowPlugin foxAggregatorShow module.
 * 
 * @package     sfFoxAggregatorShowPlugin
 * @subpackage  foxAggregatorShow
 * @author      Consulting Alpha
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasefoxAggregatorShowActions extends sfActions
{
  /**
   * Executes preExecute
   *
   * @param sfRequest $request A request object
   */
  public function preExecute()
  {

    $this->aggregator = new sfFoxAggregatorShow($this->getRequest()->getParameter('module'));
    $this->shows  = $this->aggregator->getShows();
    $this->show   = $this->aggregator->getMainShow();
    $this->show->setSynopsis("...");
  }

  function executeAbout(sfWebRequest $request)
  {
  }


  /**
   * Executes cast
   *
   * @param sfRequest $request A request object
   */
  public function executeCast(sfWebRequest $request)
  {
    $this->forward404if(!$this->aggregator->hasCasts());

    $this->casts = $this->aggregator->getCasts();
  }

  /**
   * Executes extra action
   *
   * @param sfRequest $request A request object
   */
  public function executeExtra(sfWebRequest $request)
  {
    $this->forward404Unless($this->aggregator->hasExtras());

    $this->extras = $this->aggregator->getExtras();
  }


  /**
   * Executes extra action
   *
   * @param sfRequest $request A request object
   */
  public function executeExtraDectail(sfWebRequest $request)
  {

    $this->extra = ExtraQuery::create()->findOneBySlug("/" . $this->getUser()->getCulture() ."/" . $request->getParameter('slugExtra'));
    $this->forward404if(!$this->extra);
    $this->photoes = $this->extra->getPublishedPhotoes();
    $this->videos  = $this->extra->getPublishedVideos();
    $format = ($this->extra->getFormat()) ? $this->extra->getFormat() : 'col2';
    return ucFirst($format) . sfView::SUCCESS;

  }

  /**
   * Show page
   */
  public function executeHome()
  {
  }

  /**
   * Add a new comment
   * @param sfRequest $request
   */
  public function executeComment(sfWebRequest $request)
  {
    $this->setTemplate('home');
  }

  /**
   * Add a new comment
   * @param sfRequest $request
   */
  public function executeComments(sfWebRequest $request)
  {
  }

  /**
   * List of photos / single photo
   * @param sfRequest $request
   */
  public function executePhotos(sfWebRequest $request)
  {
    $this->prevPhoto = false;
    $this->nextPhoto = false;

    $this->forward404Unless($this->aggregator->hasPhotos(), 'no photos');

    $this->photoPager = $this->aggregator->getPhotoPager($request->getParameter('page', 1));
    $selectedPhoto    = PhotoQuery::create()->findOneBySlug('/' . $this->getUser()->getCulture() . '/' . $this->getRequest()->getParameter('slugPhoto'), $this->show->getId());

    $this->allPhotos  = $this->photoPager->getResults();
    $this->firstPhoto = $this->allPhotos[0];

    if ($this->getRequest()->getParameter('slugPhoto') != null)
    {
      $this->forward404Unless($selectedPhoto && $selectedPhoto->getPublished(), 'no photo');
    }
    else
    {
      $this->selectedPhoto = $this->firstPhoto;
    }


    if (count($this->allPhotos) > 1)
    {
      $this->nextPhoto = $this->allPhotos[1];
    }

    foreach ($this->allPhotos as $k => $photo)
    {
      if ($photo == $selectedPhoto)
      {
        $this->selectedPhoto = $selectedPhoto;

        $this->prevPhoto     = isset($this->allPhotos[($k - 1)]) ? $this->allPhotos[($k - 1)] : false;
        $this->nextPhoto     = isset($this->allPhotos[($k + 1)]) ? $this->allPhotos[($k + 1)] : false;
      }
    }

    $this->forward404Unless($this->selectedPhoto && $this->selectedPhoto->getPublished(), 'no photo');

    $this->rating = $this->selectedPhoto->getRatingAverage();
  }

  /**
   * Vote photo
   * @param sfRequest $request
   */
  public function executeVotePhoto(sfWebRequest $request)
  {
    $photo = PhotoQuery::create()->findOneBySlug('/' . $this->getUser()->getCulture() . '/' . $this->getRequest()->getParameter('slugPhoto'), $this->show->getId());
    $this->forward404Unless($photo && $photo->getPublished(), 'photo not found');
    if ($photo->vote($request->getCookie('hcvp_' . $photo->getId()), $request->getParameter('vote'))->save())
    {
      $this->getResponse()->setCookie('hcvp_' . $photo->getId(), $request->getParameter('vote'), '+1 year');
    }
    $this->redirectUnless($request->isXmlHttpRequest(), '@showPhoto?slug=' . $this->show->getI18nSlug() . '&slugPhoto=' . $photo->getI18nSlug());
    $this->photo = $photo;
  }

  /**
   * Executes tvGuide action
   *
   * @param sfRequest $request A request object
   */
  public function executeTvGuide(sfWebRequest $request)
  {
    $this->programs = $this->aggregator->getTvGuide($request->getParameter('date',null), $request->getParameter('page',1), sfConfig::get('app_pager_tvguide',10));

    //$this->forward404Unless(!$this->programs);
  }

  /**
   * Executes videos action
   *
   * @param sfRequest $request A request object
   */
  public function executeVideos(sfWebRequest $request)
  {
    $this->forward404Unless($this->aggregator->hasVideos(), 'no videos');

    $this->videoPager = $this->aggregator->getVideoPager($request->getParameter('page', 1));
    $selectedVideo    = VideoQuery::create()->findOneBySlug('/' . $this->getUser()->getCulture() . '/' . $this->getRequest()->getParameter('slugVideo'), $this->show->getId());


    $this->allVideos  = $this->videoPager->getResults();
    $this->forward404If(!isset($this->allVideos[0]));

    $this->firstVideo = $this->allVideos[0];

    if ($this->getRequest()->getParameter('slugVideo') != null)
    {
      $this->forward404Unless($selectedVideo && $selectedVideo->getPublished(), 'no video');
    }
    else
    {
      $this->selectedVideo = $this->firstVideo;
    }

    if (count($this->allVideos) > 1)
    {
      $this->prevVideo = false;
      $this->nextVideo = $this->allVideos[1];
    }


    foreach ($this->allVideos as $k => $video)
    {
      if ($video == $selectedVideo)
      {
        $this->selectedVideo = $selectedVideo;
        $this->prevVideo     = isset($this->allVideos[($k - 1)]) ? $this->allVideos[($k - 1)] : false;
        $this->nextVideo     = isset($this->allVideos[($k + 1)]) ? $this->allVideos[($k + 1)] : false;
      }
    }

    if(!$this->selectedVideo instanceof Video) $this->selectedVideo  = $selectedVideo;
  
    $this->rating = $this->selectedVideo->getRatingAverage();
    $this->selectedVideo->addView()->save();
  }


  /**
   * Executes Preview action
   *
   * @param sfRequest $request A request object
   */
  public function executePreview(sfWebRequest $request)
  {
  }

  /**
   * Executes Program action
   *
   * @param sfRequest $request A request object
   */
  public function executeProgram(sfWebRequest $request)
  {

  }

  /**
   * Executes Press action
   *
   * @param sfRequest $request A request object
   */
  public function executePress(sfWebRequest $request)
  {
    
  }

}
