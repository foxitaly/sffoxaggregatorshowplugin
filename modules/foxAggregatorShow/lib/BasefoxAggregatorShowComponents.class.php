<?php

/**
 * Base components for the sfFoxAggregatorShowPlugin foxAggregatorShow module.
 * 
 * @package     sfFoxAggregatorShowPlugin
 * @subpackage  foxAggregatorShow
 * @author      Consulting Alpha
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasefoxAggregatorShowComponents extends sfComponents
{

   /**
   * Executes initialize
   *
   */
  public function initialize($context, $moduleName, $actioName)
  {

    parent::initialize($context, $moduleName, $actioName);

    $this->parser = new sfFoxAggregatorParserYml(0,sfConfig::get('sf_app_module_dir') . DIRECTORY_SEPARATOR . $this->getRequest()->getParameter('module') . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "shows.yml");

    $this->shows = ShowQuery::create()->filterById($this->parser->getIds())->find();
    $this->aggregator = new sfFoxAggregatorShow($this->getRequest()->getParameter('module'));

  }


   /**
   * Executes ShowTvGuideBox action
   *
   * @param sfRequest $request A request object
   */
  public function executeShowTvGuideBox(sfWebRequest $request)
  {
    $today = (isset($this->day)) ? $this->day : date('Y-m-d');

    $this->programs   = $this->show->getTvGuideForBox(4, $this->parser->getIds());
  }

  /**
   * Executes ShowExtraBox action
   *
   * @param sfRequest $request A request object
   */
  public function executeShowExtraBox(sfWebRequest $request)
  {
    $this->extras = ($this->show) ? $this->show->getExtrasForBox(4, $this->parser->getIds()) : Array();
  }

  /**
   * Executes ShowLatestVideosBox action
   *
   * @param sfRequest $request A request object
   */
  public function executeShowLatestVideosBox(sfWebRequest $request)
  {
    $this->videos = ($this->show) ? $this->aggregator->getLatestVideos(4) : Array();
  }

}
