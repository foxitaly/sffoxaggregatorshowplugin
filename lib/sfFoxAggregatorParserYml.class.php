<?php
class sfFoxAggregatorParserYml extends sfYamlParser
{

  private $ids = Array();
  /**
   * Constructor
   *
   * @param integer $offset The offset of YAML document (used for line numbers in error messages)
   */
  public function __construct($offset = 0, $file)
  {
    parent::__construct($offset);


    $idsFile  = $file;
    $elements = $this->parse(file_get_contents($idsFile));
    $this->ids = $elements['fox_aggregator_show']['ids'];
  }

  public function getIds()
  {
    return $this->ids;
  }

}
