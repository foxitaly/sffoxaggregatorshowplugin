<?php

class sfFoxAggregatorShow 
{

  private $shows, $show, $ids; 

  /**
   * construct
   *
   * @params string $moduleName
   */
  public function __construct($moduleName)
  {
    $parser = new sfFoxAggregatorParserYml(0,sfConfig::get('sf_app_module_dir') . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "shows.yml");

    $this->setShowIds($parser->getIds());

    $this->setShows(ShowQuery::create()->filterById($this->getShowIds())->find());

    $this->setMainShow(new Show);
  }

  /**
   * setShowIds
   *
   */
  private function setShowIds($s)
  {
    $this->ids = $s;
  }

  /**
   * getShowIds
   *
   */
  public function getShowIds()
  {
    return $this->ids;
  }


  /**
   * setMainShow
   *
   */
  private function setMainShow($s)
  {
    $this->show = $s;
  }

  /**
   * getMainShow
   *
   */
  public function getMainShow()
  {
    return $this->show;
  }

  /**
   * setShows
   *
   */
  private function setShows($s)
  {
    $this->shows = $s;
  }

  /**
   * getShows
   *
   */
  public function getShows()
  {
    return $this->shows;
  }


  /**
   * hasPhotos
   *
   */
  public function hasPhotos()
  {
    return true;
  }

  /**
   * hasSynopsis()
   *
   */
  public function hasSynopsis()
  {
    return true;
  }


  /**
   * hasVideos
   *
   */
  public function hasVideos()
  {
    return true;
  }

  /**
   * hasExtras
   *
   */
  public function hasExtras()
  {
    return true;
  }

  /**
   * hasCasts
   *
   */
  public function hasCasts()
  {
    return false;
  }

  /**
   * hasNews
   *
   */
  public function hasNews()
  {
    return false;
  }

  /**
   * hasTvGuide
   *
   */
  public function hasTvGuide()
  {
    return true;
  }

  /**
   * hasCard() must be return true if show has associated Photo or Video or Cast or Extra false otherwise
   * @return boolean
   */
  public function hasCard()
  {
    return $this->hasPhotos() || $this->hasVideos() || $this->hasCasts()
      || $this->hasExtras() || $this->hasSynopsis();
  }


  /**
   * Check if show is airing
   * @return boolean
   */
  public function isAiring()
  {
    return true;
  }


  /** 
   * getCharacters
   *
   * @params Criteria $c
   */
  public function getCasts()
  {
    $casts = CharacterQuery::create()->filterByShowId($this->getShowIds())->orderBySortableRank('DESC')->find(); 
    return $casts;
  }

  /** 
   * getExtras
   *
   * @params Criteria $c
   */
  public function getExtras()
  {
    $extras = ExtraQuery::create()->filterByShowId($this->getShowIds())->find(); 
    return $extras;
  }

  /**
   * getTvGuide
   *
   * @params string  $date
   * @params integer $page
   * @params integer $maxPerPage
   * @params array $onlyThisShows
   */
  public function getTvGuide($date, $page, $maxPerPage, $onlyThisShows = Array())
  {
    if(count($onlyThisShows)) return ShowQuery::create()->getTvGuide($onlyThisShows,$date)->paginate($page, $maxPerPage);

    return ShowQuery::create()->getTvGuide($this->getShowIds(),$date)->paginate($page, $maxPerPage);  
  }

  /**
   * getPhotoPager
   *
   * @params integer $page
   */
  public function getPhotoPager($page)
  { 
    return ShowQuery::create()->getPhotos($this->getShowIds())->paginate($page, sfConfig::get('app_pager_photo', 24));

  }

  /**
   * getTvGuideForBox
   *
   * @params integer $page
   */
  public function getTvGuideForBox($page=0)
  { 
    return;
  }


  /**
   * getVideoPager
   *
   * @params integer $page
   */
  public function getVideoPager($page)
  { 
    return ShowQuery::create()->getVideos($this->getShowIds())->paginate($page, sfConfig::get('app_pager_video',24));
  }

  /**
   * getLatestVideo
   *
   * @params integer $max
   * @params boolean $onlyPromo
   */
  public function getLatestVideos($max,$onlyPromo = false)
  { 
    return ShowQuery::create()->getVideos($this->getShowIds())->
    _if($onlyPromo)->
      filterByIsPromo(true)->
    _endif()->
    orderById('DESC')->limit($max)->find();
  }

}
